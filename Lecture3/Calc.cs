﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture3
{
    public static class Calc
    {
        public static double Plus(double a, double b)
        {
            return a + b;
        }

        public static double Minus(double a, double b)
        {
            return a - b;
        }

        public static double Multiply(double a, double b)
        {   
            return a * b;
        }

        public static double Divide(double a, double b)
        {
            if (b == 0)
            {
                throw new ArgumentException("Division by zero");
            }
            return a / b;
        }

        public static double Power(double a, double b)
        {
            return Math.Pow(a, b);
        }

        public static void DoOperation(string[] elements)
        {
            var a = Convert.ToDouble(elements[0]);
            char operation = elements[1][0];
            var b = Convert.ToDouble(elements[2]);


            switch (operation)
            {
                case '+':
                    Console.Write(Plus(a, b));
                    break;
                case '-':
                    Console.Write(Minus(a, b));
                    break;
                case '*':
                    Console.Write(Multiply(a, b));
                    break;
                case '/':
                    Console.Write(Divide(a, b));
                    break;
                case '^':
                    Console.Write(Power(a, b));
                    break;
                default:
                    throw new Exception("Somethig is wrong");
            }
        }

        public static bool Validate(string inputStr)
        {
            char[] operations = new char[] { '+', '-', '*', '/', '^' };
            var elements = inputStr.Split(new char[] { ' ' });

            if (elements.Length != 3)
            {
                if (elements.Length > 3)
                {
                    throw new Exception("Too many input elements");
                    return false;
                }
                else
                {
                    throw new Exception("Need more input elements");
                    return false;
                }
            }

            if (elements[1].Length != 1)
            {
                throw new Exception("Invalid operator");
                return false;
            }

            if (!operations.Contains(elements[1][0]))
            {
                throw new Exception("Invalid operator");
                return false;
            }

            return true;
        }
    }
}
