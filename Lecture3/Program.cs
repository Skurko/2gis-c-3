﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;

namespace Lecture3
{
    class Program
    {
        static void Task1()
        {
            var inputText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quis ligula diam. Mauris rutrum est et aliquam porttitor. Nunc molestie bibendum condimentum. Curabitur eu volutpat purus. Maecenas eu luctus libero. Aenean enim metus, fermentum ut mauris eget, feugiat suscipit magna. Donec lobortis mauris a sapien accumsan, nec lobortis tortor pulvinar. Phasellus consequat elementum vehicula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id pretium tellus. Proin id turpis sit amet metus elementum porttitor quis in massa. Integer in metus sit amet libero laoreet suscipit. Ut nec volutpat elit, sed tincidunt massa. Nulla tristique libero in gravida condimentum. Nam nisl justo, vestibulum nec dignissim nec, pretium eget magna. Quisque nulla nisl, egestas et elit eget, eleifend ultrices lorem. Aenean dolor arcu, suscipit sed metus eu, mattis sagittis enim. Vestibulum eget pulvinar nisi, non convallis lorem. In porttitor elit erat, id dapibus nisi lobortis in. Aliquam risus nulla, ultrices eu magna id, auctor vehicula sapien. Interdum et malesuada fames ac ante ipsum primis in faucibus. Praesent et urna aliquet, auctor lacus in, vestibulum mauris. Sed ipsum mi, tristique id velit nec, porttitor scelerisque mauris. Ut sagittis non ante condimentum tristique. Praesent efficitur, dolor non euismod consequat, metus elit molestie erat, vel posuere nisi dolor ac tortor. Proin vehicula ex in lorem scelerisque, vitae commodo magna mollis. Fusce non nibh quis nisi sodales efficitur quis eget dolor. Nunc nisl nibh, aliquet eget consequat vitae, viverra a est. Nam quis ex vitae lorem tincidunt pharetra et eget urna. Sed in ligula et enim ullamcorper fringilla. Fusce urna libero, blandit ac malesuada interdum, dictum sed justo. Nunc tellus turpis, malesuada lacinia mi malesuada, aliquam vulputate velit. Maecenas efficitur, nibh id dapibus luctus, metus augue porttitor nisi, ac malesuada leo enim vel purus. Donec elementum sapien rhoncus erat gravida interdum. Cras suscipit turpis sollicitudin nibh cursus, a tincidunt sapien fringilla. Sed ut mauris felis. Fusce non nisi eget erat commodo tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse pharetra pharetra dui tempor elementum. Aenean ac lacus nisl. Pellentesque tristique odio sed convallis bibendum. Sed eget scelerisque nisi, vitae luctus tortor. Aenean vestibulum scelerisque augue, et porta ipsum commodo.";
            
            Console.WriteLine("Text:\n" + inputText);
            inputText = inputText.Trim();
            var words = inputText.Split(new char[] {' ', '.', ',', ':', '—', '!', '?',});
            var emptyStrings = 0;
            for (var i = 0; i < words.Length; i++)
            {
                if ((words[i] == "") || (words[i] == "-"))
                {
                    emptyStrings++;
                }
            }
            Console.WriteLine("Всего слов в тексте: " + (words.Length - emptyStrings));
            Console.ReadKey();
        }

        //Подсчитать количество вхождения каждого слова в тексте и вывести на экран
        static void Task2()
        {
            var inputText = "Иван - роДил девчонку велел, велел тащить пелёнку! Иван. ивАН: родил? родиЛ";

            Console.WriteLine("Text:\n" + inputText);
            inputText = inputText.Trim().ToLower();
            var words = inputText.Split(new char[] { ' ', '.', ',', ':', '—', '!', '?', });

            var countedWords = new Dictionary<string, int>();

            for (var i = 0; i < words.Length; i++)
            {
                if ((words[i] != "") && (words[i] != "-"))
                {
                    if (countedWords.ContainsKey(words[i]))
                    {
                        countedWords[words[i]] = countedWords[words[i]] + 1;
                    }
                    else
                    {
                        countedWords.Add(words[i], 1);
                    }
                }
            }

            var keys = countedWords.Keys;

            for (var i = 0; i < keys.Count; i++)
            {
                keys.ElementAt(1);
                Console.WriteLine("{0}: {1}", keys.ElementAt(i), countedWords[keys.ElementAt(i)]);
            }

            Console.ReadKey();
        }

        //Калькулятор где все операнды вводим в одну строку, разделитель – пробел. Оформить все операции в качестве функций.
        static void Task3()
        {
            var inputStr = Console.ReadLine();

            if (Calc.Validate(inputStr))
            {
                var elements = inputStr.Split(new char[] { ' ' });
                Calc.DoOperation(elements);
            }

            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            const string LINE = "----------------------------";
            
            Console.WriteLine("Заполнить матрицу NxN случайными числами до 100. Найти минимум в каждой строке.");
            Task1();
            Console.WriteLine(LINE);
            

            Console.WriteLine("Подсчитать количество вхождения каждого слова в тексте и вывести на экран");
            Task2();
            Console.WriteLine(LINE);
            
            
            Console.WriteLine("Калькулятор где все операнды вводим в одну строку, разделитель – пробел. Оформить все операции в качестве функций.");
            try
            {
                Task3();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString() + e.Message);
                Console.ReadKey();
            }
            Console.WriteLine(LINE);
        }
    }
}
